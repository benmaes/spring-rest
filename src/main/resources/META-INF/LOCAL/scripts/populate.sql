INSERT INTO regions(region_id, region_name) VALUES (1, 'Europe');
INSERT INTO regions(region_id, region_name) VALUES (2, 'Americas');
INSERT INTO regions(region_id, region_name) VALUES (3, 'Asia');

INSERT INTO countries(country_id, country_name, region_id) VALUES ('AR', 'Argentina', 2);
INSERT INTO countries(country_id, country_name, region_id) VALUES ('AU', 'Australia', 3);
INSERT INTO countries(country_id, country_name, region_id) VALUES ('BE', 'Belgium', 1);
INSERT INTO countries(country_id, country_name, region_id) VALUES ('BR', 'Brazil', 2);
INSERT INTO countries(country_id, country_name, region_id) VALUES ('CA', 'Canada', 2);
INSERT INTO countries(country_id, country_name, region_id) VALUES ('CH', 'Switzerland', 1);

INSERT INTO locations(location_id, street_address, postal_code, city, state_province, country_id) VALUES (1000, 'Veldkant 33a', '2660', 'Kontich', 'Antwerpen', 'BE');
INSERT INTO locations(location_id, street_address, postal_code, city, state_province, country_id) VALUES (1500, 'Diamantstraat 8', '2550', 'Herentals', 'Antwerpen', 'BE');

INSERT INTO departments(department_id, department_name, location_id) VALUES (60, 'IT', 1000);
INSERT INTO departments(department_id, department_name, location_id) VALUES (40, 'HR', 1000);
INSERT INTO departments(department_id, department_name, location_id) VALUES (70, 'PR', 1000);
INSERT INTO departments(department_id, department_name, location_id) VALUES (80, 'Sales', 1500);

INSERT INTO jobs(job_id, job_title, min_salary, max_salary) VALUES ('IT_PROG', 'Programmer', 4000, 10000);
INSERT INTO jobs(job_id, job_title, min_salary, max_salary) VALUES ('AC_MGR', 'Account Manager', 8200, 16000);
INSERT INTO jobs(job_id, job_title, min_salary, max_salary) VALUES ('SA_MAN', 'Sales Manager', 10000, 20000);
INSERT INTO jobs(job_id, job_title, min_salary, max_salary) VALUES ('AD_ASST', 'Administration Assistant', 3000, 6000);

INSERT INTO employees(employee_id, last_name, first_name, email, phone_number, hire_date, job_id, salary, commission_pct, department_id) VALUES (100, 'Maes', 'Ben', 'ben.maes@c4j.be', '123', TO_DATE('30-SEP-2013', 'DD-MON-YYYY'), 'IT_PROG', 6000, null, 60);
INSERT INTO employees(employee_id, last_name, first_name, email, phone_number, hire_date, job_id, salary, commission_pct, department_id) VALUES (101, 'De Weerdt', 'Ben', 'ben.de.weerdt@c4j.be', '456', TO_DATE('01-SEP-2014', 'DD-MON-YYYY'), 'IT_PROG', 4000, null, 60);
INSERT INTO employees(employee_id, last_name, first_name, email, phone_number, hire_date, job_id, salary, commission_pct, department_id) VALUES (102, 'Maesschalck', 'Wim', 'wim.maesschalck@cosmoz.be', '789', TO_DATE('01-JUL-2014', 'DD-MON-YYYY'), 'IT_PROG', 5000, null, 60);
INSERT INTO employees(employee_id, last_name, first_name, email, phone_number, hire_date, job_id, salary, commission_pct, department_id) VALUES (103, 'Vermeire', 'Anja', 'anja.vermeire@icross.be', '987', TO_DATE('01-FEB-2010', 'DD-MON-YYYY'), 'AD_ASST', 5000, null, 40);
INSERT INTO employees(employee_id, last_name, first_name, email, phone_number, hire_date, job_id, salary, commission_pct, department_id) VALUES (104, 'Stroobants', 'Tom', 'tom.stroobants@c4j.be', '654', TO_DATE('01-APR-2008', 'DD-MON-YYYY'), 'AC_MGR', 9000, 0.5, 70);
INSERT INTO employees(employee_id, last_name, first_name, email, phone_number, hire_date, job_id, salary, commission_pct, department_id) VALUES (105, 'Benats', 'Jeroen', 'jeroen.benats@c4j.be', '321', TO_DATE('15-AUG-2009', 'DD-MON-YYYY'), 'SA_MAN', 10000, 0.9, 80);

UPDATE departments SET manager_id = 105 WHERE department_id = 60;
UPDATE departments SET manager_id = 104 WHERE department_id = 40;
UPDATE departments SET manager_id = 104 WHERE department_id = 70;
UPDATE departments SET manager_id = 100 WHERE department_id = 80;

UPDATE employees SET manager_id = 105 WHERE employee_id = 100;
UPDATE employees SET manager_id = 105 WHERE employee_id = 101;
UPDATE employees SET manager_id = 105 WHERE employee_id = 102;
UPDATE employees SET manager_id = 104 WHERE employee_id = 103;