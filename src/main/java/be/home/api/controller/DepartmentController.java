package be.home.api.controller;

import be.home.api.entity.Department;
import be.home.api.resource.DepartmentResource;
import be.home.api.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/departments")
public class DepartmentController {

    @Autowired
    private DepartmentService departmentService;

    @RequestMapping(method = RequestMethod.GET)
    public List<Department> getAllDepartments() {
        return departmentService.getDepartments();
    }

    @RequestMapping(value = "/{departmentId}", method = RequestMethod.GET)
    public Department getDepartmentById(@PathVariable int departmentId) {
        return departmentService.getDepartmentById(departmentId);
    }
}
