package be.home.api.service;

import be.home.api.entity.Employee;

import java.util.List;

public interface EmployeeService {
    List<Employee> getEmployees();
    List<Employee> getEmployeesByDepartmentId(int id);
    Employee getEmployeeById(int id);
}
