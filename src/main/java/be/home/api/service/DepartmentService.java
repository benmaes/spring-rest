package be.home.api.service;


import be.home.api.entity.Department;

import java.util.List;

public interface DepartmentService {
    List<Department> getDepartments();
    Department getDepartmentById(int id);


}
