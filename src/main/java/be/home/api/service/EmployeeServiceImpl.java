package be.home.api.service;

import be.home.api.entity.Employee;
import be.home.api.resource.EmployeeResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeResource employeeResource;

    @Override
    public List<Employee> getEmployees() {
        return employeeResource.getEmployees();
    }

    @Override
    public List<Employee> getEmployeesByDepartmentId(int id) {
        return employeeResource.getEmployeesByDepartmentId(id);
    }

    @Override
    public Employee getEmployeeById(int id) {
        return employeeResource.getEmployeeById(id);
    }
}
