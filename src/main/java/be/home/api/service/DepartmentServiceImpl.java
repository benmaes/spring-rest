package be.home.api.service;

import be.home.api.entity.Department;
import be.home.api.entity.Employee;
import be.home.api.resource.DepartmentResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Service
public class DepartmentServiceImpl implements DepartmentService {

    @Autowired
    private DepartmentResource departmentResource;

    @Autowired
    private EmployeeService employeeService;

    @Override
    public List<Department> getDepartments() {
        List<Department> departments = departmentResource.getDepartments();

        for(Department department : departments) {
            department.setEmployees(getEmployeesForDepartment(department.getDepartmentId()));
        }

        return departments;
    }

    @Override
    public Department getDepartmentById(int id) {
        Department department = departmentResource.getDepartmentById(id);

        department.setEmployees(getEmployeesForDepartment(id));
        return department;
    }

    private List<Employee> getEmployeesForDepartment(int id) {
        return employeeService.getEmployeesByDepartmentId(id);
    }
}
