package be.home.api.interceptor;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

@Aspect
@Component
public class EndpointLoggingInterceptor {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Before("execution(* be.home.api.controller.*Controller.*(..))")
    public void logEndpoint(JoinPoint joinPoint) {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();

        StringBuilder sb = new StringBuilder();
        RequestMapping classAnnotation = (RequestMapping) signature.getDeclaringType().getDeclaredAnnotation(RequestMapping.class);
        sb.append(classAnnotation.value()[0]);

        Method method = signature.getMethod();
        RequestMapping methodAnnotation = method.getDeclaredAnnotation(RequestMapping.class);
        String httpVerb = methodAnnotation.method()[0].name();
        if(methodAnnotation.value().length > 0) {
            sb.append(methodAnnotation.value()[0]);
        }

        LOGGER.info(String.format("%s request was done to %s", httpVerb, sb.toString()));
    }
}
