package be.home.api.resource;

import be.home.api.entity.Employee;

import java.util.List;

public interface EmployeeResource {
    List<Employee> getEmployees();
    List<Employee> getEmployeesByDepartmentId(int id);
    Employee getEmployeeById(int id);
}
