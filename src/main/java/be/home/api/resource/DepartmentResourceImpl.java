package be.home.api.resource;

import be.home.api.entity.Department;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import java.util.List;

@Repository
public class DepartmentResourceImpl implements DepartmentResource {

    @Autowired
    private EntityManagerFactory entityManagerFactory;

    @Override
    public List<Department> getDepartments() {
        Query query = getEntityManager().createQuery("SELECT d FROM Department d");
        return query.getResultList();
    }


    @Override
    public Department getDepartmentById(int id) {
        Query query = getEntityManager().createQuery("SELECT d FROM Department d WHERE d.departmentId = :id");
        query.setParameter("id", id);
        return (Department) query.getSingleResult();
    }

    private EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }
}
