package be.home.api.resource;

import be.home.api.entity.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import java.util.List;

@Repository
public class EmployeeResourceImpl implements EmployeeResource {

    @Autowired
    private EntityManagerFactory entityManagerFactory;

    public List<Employee> getEmployees() {
        Query query = getEntityManager().createQuery("SELECT e FROM Employee e");
        return query.getResultList();
    }

    @Override
    public List<Employee> getEmployeesByDepartmentId(int id) {
        Query query = getEntityManager().createQuery("SELECT e FROM Employee e WHERE e.department.departmentId = :id");
        query.setParameter("id", id);
        return query.getResultList();
    }

    @Override
    public Employee getEmployeeById(int id) {
        Query query = getEntityManager().createQuery("SELECT e FROM Employee e WHERE e.employeeId = :id");
        query.setParameter("id", id);
        return (Employee) query.getSingleResult();
    }

    private EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }
}
