package be.home.api.resource;

import be.home.api.entity.Department;

import java.util.List;

public interface DepartmentResource {
    List<Department> getDepartments();
    Department getDepartmentById(int id);
}
